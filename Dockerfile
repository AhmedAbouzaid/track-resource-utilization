FROM python:3.7
RUN echo "Acquire::Check-Valid-Until \"false\";\nAcquire::Check-Date \"false\";" | cat > /etc/apt/apt.conf.d/10no--check-valid-until
RUN apt-get update && apt-get -y install cron vim 
RUN apt-get -y install python3-pip
RUN pip install --user psutil
RUN pip3 install --user psutil

WORKDIR /app
COPY crontab /etc/cron.d/crontab
COPY demo.py /app/demo.py
RUN chmod 0644 /etc/cron.d/crontab
RUN /usr/bin/crontab /etc/cron.d/crontab

# run crond as main process of container
CMD ["cron", "-f"]
