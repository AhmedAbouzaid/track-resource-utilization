import psutil
import os
import csv
import time
from pathlib import Path

#CPU usage in a one-second-interval 
cpu = psutil.cpu_percent(1)

# Memory usage
mem = (psutil.virtual_memory()[1]/psutil.virtual_memory()[0])*100 #available/total

#Disk usage
disk = (psutil.disk_usage('/')[2]/psutil.disk_usage('/')[0])*100

now = time.strftime('%d-%m-%Y %H:%M:%S')

#Write CPU data
fieldnames = ['Timestamp', 'CPU Utilization']
cpu_csv = Path('/opt/CPU.csv')
if cpu_csv.is_file():
    with open(cpu_csv,'a') as c:
        writer = csv.DictWriter(c, fieldnames=fieldnames)
        writer.writerow({'Timestamp': now, 'CPU Utilization': cpu})
else:
    with open(cpu_csv, 'w', newline="") as f:
        writer = csv.DictWriter(f, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerow({'Timestamp': now, 'CPU Utilization': cpu})

print('Timestamp: ', now, '  ', 'CPU Utilization: ',  cpu)

#Write Memory data
fieldnames = ['Timestamp', 'Free Memory Percentage']
mem_csv = Path('/opt/MEM.csv')
if mem_csv.is_file():
    with open(mem_csv,'a') as c:
        writer = csv.DictWriter(c, fieldnames=fieldnames)
        writer.writerow({'Timestamp': now, 'Free Memory Percentage': mem})
else:
    with open(mem_csv, 'w', newline="") as f:
        writer = csv.DictWriter(f, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerow({'Timestamp': now, 'Free Memory Percentage': mem})

print('Timestamp: ', now, '  ', 'Free Memory Percentage: ',  mem)

#Write Disk data
fieldnames = ['Timestamp', 'Free Disk Percentage']
disk_csv = Path('/opt/DISK.csv')
if disk_csv.is_file():
    with open(disk_csv,'a') as c:
        writer = csv.DictWriter(c, fieldnames=fieldnames)
        writer.writerow({'Timestamp': now, 'Free Disk Percentage': disk})
else:
    with open(disk_csv, 'w', newline="") as f:
        writer = csv.DictWriter(f, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerow({'Timestamp': now, 'Free Disk Percentage': disk})

print('Timestamp: ', now, '  ', 'Free Disk Percentage: ',  disk)

