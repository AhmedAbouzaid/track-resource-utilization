# Track Resource Utilization

This script is to automatically record system resources utilization on a Linux machine. It generates 3 CSVs files under /opt/ directory.
- CPU.csv
- MEM.csv [Free memory percentage ]
- DISK.csv [ Root disk available space percentage ]

Each row has two columns timestamp and utilization at that time, which is triggered every 15 minutes.

# Build Docker
docker build -t track-resource-utilization .

# Run docker container
docker run -it --rm track-resource-utilization

